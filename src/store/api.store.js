import { defineStore } from "pinia";
import { useMainStore } from "./index.store";

const API_URL = "https://api.sna.jerald.dev";

const API_CALL = async (method, url, payload) => {
  let options = {
    method,
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
  };
  switch (method.toUpperCase()) {
    case "GET": {
      return await (await fetch(API_URL + url, options)).json();
    }
    case "POST": {
      options.data = JSON.stringify(payload);
      return await (await fetch(API_URL + url, options)).json();
    }
    case "PUT": {
      options.data = JSON.stringify(payload);
      return await (await fetch(API_URL + url, options)).json();
    }
    case "DELETE": {
      options.data = JSON.stringify(payload);
      return await (await fetch(API_URL + url, options)).json();
    }
    default: {
      throw Error("Inavlid Method");
    }
  }
};

export const useApiStore = defineStore({
  id: "ApiStore",
  state: () => ({
    countries: [],
    selectedCountry: {},
    states: [],
  }),
  getters: {
    COUNTRY: (state) => state.countries,
    COUNTRY_SELECTED: (state) => state.selectedCountry,
    STATES: (state) => state.states,
  },
  actions: {
    async GET_COUNTRY() {
      let mainStore = useMainStore();

      mainStore.setLoading(true);
      try {
        this.countries = await API_CALL("GET", "/countries?_limit=10");
      } catch (error) {
        mainStore.error({
          message: error.message,
          title: "Something went worng",
        });
      } finally {
        mainStore.setLoading(false);
      }
    },
    async GET_STATES(id) {
      let mainStore = useMainStore();

      mainStore.setLoading(true);
      try {
        let data = await Promise.all([
          await API_CALL("GET", "/countries?id=" + id),
          await API_CALL("GET", "/states?country_id=" + id),
        ]);
        this.selectedCountry = data[0][0];
        this.states = data[1];
      } catch (error) {
        mainStore.error({
          message: error.message,
          title: "Something went worng",
        });
      } finally {
        mainStore.setLoading(false);
      }
    },
  },
});
