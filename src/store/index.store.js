import { defineStore } from "pinia";

export const useMainStore = defineStore({
  id: "mainStore",
  state: () => ({
    alert: {
      isError: false,
      title: "",
      message: "",
      type: "",
    },
    loading: false,
  }),
  getters: {
    isLoading: (state) => state.loading,
    isError: (state) => state.alert.isError,
    Errors: (state) => state.alert,
  },
  actions: {
    setLoading(data) {
      this.loading = data;
    },
    success({ message, title }) {
      this.alert = { title, isError: true, message, type: "success" };
    },
    error({ message, title }) {
      this.alert = { title, isError: true, message, type: "error" };
    },
    clear() {
      this.alert = {
        isError: false,
        message: "",
        type: "",
        title: "",
      };
    },
  },
});
