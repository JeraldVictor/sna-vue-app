import { createRouter, createWebHistory } from "vue-router";

import Home from "./pages/index.vue";
import States from "./pages/States.vue";

const routes = [
  { path: "/", component: Home },
  {
    path: "/states/:id",
    component: States,
  },
];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
});

export default router;
